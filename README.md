# How to run

```shell
$ git clone https://bitbucket.org/ydm/iqv.git
$ virtualenv env
$ . scripts/myenv.sh
$ pip install -r requirements.txt
$ flask db upgrade
$ flask run
$ # or
$ ./scripts/test.sh
$ # or
$ cd src
$ BROKER_URI=redis://host:port/x celery worker -A tasks --beat
```

# Run Celery tasks
```
$ ./scripts/task.sh [task_name]
```

task_name may be one of:

- delete_older_contacts
- create_random_contact

If omitted, task_name defaults to delete_older_contacts.

Optionally you can run Celery tasks manually from the interpreter:

```shell
$ . scripts/myenv.sh
$ cd src
$ python -c 'import tasks; tasks.delete_older_contacts()'
$ # or
$ python
...
>>> import tasks
>>> tasks.delete_older_contacts()
```
