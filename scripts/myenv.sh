#!/bin/bash

function os {
    # args: gnu, osx, win
    if   [ $OSTYPE = "linux-gnu" ] ; then echo "$1" ;
    elif [[ $OSTYPE == darwin* ]]  ; then echo "$2" ;
    else                                  echo "$3" ; fi
}

. $(os env/bin/activate env/bin/activate env/Scripts/activate)

export FLASK_APP=src/iqv/boot.py
export FLASK_ENV=development
