#!/bin/bash

curl localhost:5000/contacts -XPOST \
     -Fusername=user \
     -Ffirst_name=first \
     -Flast_name=last \
     -Femails='one@example.com' \
     -Femails='two@example.com'
