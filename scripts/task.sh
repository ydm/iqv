#!/bin/bash

TASK=${1:-delete_older_contacts}

. scripts/myenv.sh

(
    cd src
    PROGN="import tasks; tasks.$TASK()"
    python -c "$PROGN"
)
