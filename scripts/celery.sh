#!/bin/bash

. scripts/myenv.sh

redis-server --port 7777 &

# Sleep a bit to make sure the redis sever is initialized
sleep 5

export BROKER_URI=redis://localhost:7777/0

(
    cd src
    celery worker -A tasks --beat --loglevel=info
)
