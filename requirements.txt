Flask==1.1.1
Flask_Migrate==2.5.2
Flask_SQLAlchemy==2.4.1
Flask_RESTful==0.3.7
pytest==5.3.2
celery==4.4.0
redis==3.3.11
