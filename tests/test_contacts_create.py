def test_create(client, make_contact, N=10):
    '''
    Test the creation of valid objects.
    '''
    ids = set()
    for i in range(N):
        obj = make_contact(i)
        # Request
        resp = client.post('/contacts', data=obj)
        assert resp.status_code == 200
        # Inspect object
        made = resp.json['contact']
        for k, v in obj.items():
            assert made[k] == v
        assert type(made['id']) is int
        ids.add(made['id'])
    # Make sure each of the created objects has a unique ID
    assert len(ids) == N


def test_required_fields(client, make_contact):
    '''
    There are required fields that should be always present in the
    POSTed data and non-empty.  Status code 400 and an error message
    should be served on error.
    '''
    required = ('username', 'first_name')
    for field in required:
        # Should be present
        x = make_contact()
        del x[field]
        first = client.post('/contacts', data=x)
        assert first.status_code == 400
        assert field in first.json['message']
        # Should not be empty
        y = make_contact(**{field: ''})
        second = client.post('/contacts', data=y)
        assert second.status_code == 400
        assert field in second.json['message']


def test_optional_fields(client, make_contact):
    '''
    Optional fields may be omitted or empty on creation.
    '''
    def f(data, field):
        resp = client.post('/contacts', data=data)
        assert resp.status_code == 200
        assert resp.json['contact'][field] == ''
        # Make sure the rest of the data is written correctly
        for k, v in data.items():
            assert resp.json['contact'][k] == v

    optional = ('last_name',)
    for field in optional:
        # May be omitted
        x = make_contact()
        del x[field]
        f(x, field)
        # May be empty
        y = make_contact(**{field: ''})
        f(y, field)


def test_validation_username(client, make_contact):
    '''
    The username field is restricted to latin alphabet letters,
    numbers and underscores.
    '''
    xs = ('#notok', 'notok#', 'not#ok', 'not!ok', 'not@ok', 'not ok',
          'не_ок', '悪い')
    for x in xs:
        obj = make_contact(username=x)
        resp = client.post('/contacts', data=obj)
        assert resp.status_code == 400
        assert 'username' in resp.json['message']


def test_longer_username(client, make_contact):
    '''
    username shouldn't be longer than 31 characters
    '''
    longer = '123456789_123456789_123456789_12'
    obj = make_contact(username=longer)
    resp = client.post('/contacts', data=obj)
    assert resp.status_code == 400
    assert 'username' in resp.json['message']


def test_validation_first_name(client, make_contact):
    '''The only requirement for the first_name is it's not empty'''
    obj = make_contact(first_name='a')
    resp = client.post('/contacts', data=obj)
    assert resp.status_code == 200
    assert resp.json['contact']['first_name'] == 'a'


def test_nonexistent_fields(client, make_contact):
    obj = make_contact(nonexistent='something')
    resp = client.post('/contacts', data=obj)
    assert resp.status_code == 400
    assert resp.is_json
    assert 'message' in resp.json
