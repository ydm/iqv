ONE   =   'one@example.com'
TWO   =   'two@example.com'
THREE = 'three@example.com'


def assert_num_contacts(client, n):
    resp = client.get(f'/contacts?per_page=100')
    assert resp.status_code == 200
    assert len(resp.json['contact_list']) == n


def create_contact(client, make_contact):
    obj = make_contact()
    resp = client.post('/contacts', data=obj)
    assert resp.status_code == 200
    return dict(resp.json['contact'])


def test_get(client, make_contact):
    '''
    First create, then get a single contact and check its data.
    '''
    obj = create_contact(client, make_contact)
    id_ = obj['id']
    resp = client.get(f'/contact/{id_}')
    assert resp.status_code == 200
    for k, v in obj.items():
        assert resp.json['contact'][k] == v


def test_update(client, make_contact):
    '''
    Update all data fields of a contact at once.
    '''
    obj = create_contact(client, make_contact)
    id_ = obj['id']
    expected = {
        'username': 'new_username',
        'first_name': 'new first name',
        'last_name': 'new last name',
    }
    resp = client.put(f'/contact/{id_}', data=expected)
    assert resp.status_code == 200
    assert resp.json['contact']['id'] == id_
    for k, v in expected.items():
        assert resp.json['contact'][k] == v


def test_update_partial(client, make_contact):
    '''
    Update a single data field of a contact.
    '''
    FIRST_NAME = 'XXX'
    obj = create_contact(client, make_contact)
    id_ = obj['id']
    expected = {
        'id': id_,
        'username': obj['username'],
        'first_name': FIRST_NAME,
        'last_name': obj['last_name'],
    }
    uri = f'/contact/{id_}'
    x = client.put(uri, data={'first_name': FIRST_NAME})
    assert x.status_code == 200
    for k, v in expected.items():
        assert x.json['contact'][k] == v
    y = client.get(uri)
    assert y.status_code == 200
    for k, v in expected.items():
        assert y.json['contact'][k] == v


def test_update_nonexistent(client, make_contact):
    '''
    Make sure the service signals an error when trying to update a
    nonexistent field.
    '''
    obj = create_contact(client, make_contact)
    id_ = obj['id']
    nonexistent = ('th3r3', '!$', 'n0_sp00n', 'ти', '#казвам')
    for field in nonexistent:
        resp = client.put(f'/contact/{id_}', data={field: 'something'})
        assert resp.status_code == 400
        assert resp.is_json
        assert 'message' in resp.json


def test_delete(client, make_contact):
    '''
    Create some contacts, then delete them one by one and check the
    contents of what's deleted.
    '''
    xs = [create_contact(client, make_contact) for _ in range(10)]
    num = len(xs)
    while num:
        assert_num_contacts(client, num)
        x = xs.pop()
        id_ = x['id']
        uri = f'/contact/{id_}'
        # Make sure the contact exists
        exists = client.get(uri)
        assert exists.status_code == 200
        # First request should delete the contact and return its data
        first = client.delete(uri)
        assert first.status_code == 200
        assert first.json['contact'] == x
        # Make sure the contact doesn't exist anymore
        gone = client.get(uri)
        assert gone.status_code == 404
        assert 'message' in gone.json
        # Second request to delete should also fail with 404
        second = client.delete(uri)
        assert second.status_code == 404
        assert second.is_json
        assert 'message' in second.json
        num -= 1
    assert_num_contacts(client, 0)


def test_post_with_emails(client, make_contact):
    def check(resp):
        assert resp.status_code == 200
        assert len(resp.json['contact']['emails']) == 2
        assert ONE in resp.json['contact']['emails']
        assert TWO in resp.json['contact']['emails']
    obj = make_contact(emails=[ONE, TWO])
    # POST
    post = client.post('/contacts', data=obj)
    check(post)
    # GET
    id_ = post.json['contact']['id']
    get = client.get(f'/contact/{id_}')
    check(get)
    return id_


def test_put_emails(client, make_contact):
    def check(resp):
        assert resp.status_code == 200
        assert resp.json['contact']['emails'] == [THREE]
    id_ = test_post_with_emails(client, make_contact)
    uri = f'/contact/{id_}'
    obj = make_contact(emails=[THREE,])
    put = client.put(uri, data=obj)
    check(put)
    get = client.get(uri)
    check(get)
