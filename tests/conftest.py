import os
from contextlib import contextmanager
from tempfile import mkstemp

from pytest import fixture

from iqv.boot import app
from iqv import db


@contextmanager
def temp():
    fd, fn = mkstemp()
    try:
        yield fn
    finally:
        try:
            os.close(fd)
        except:
            pass
        os.unlink(fn)        


@fixture
def client():
    with temp() as fn:
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{}'.format(fn)
        app.config['TESTING'] = True
        db.create_all()
        yield app.test_client()


@fixture
def make_contact():
    def f(index=0, **kwargs):
        ans = {
            'username': f'username_{index}',
            'first_name': f'first{index}',
            'last_name': f'last{index}'
        }
        for k, v in kwargs.items():
            ans[k] = v
        return ans
    return f
