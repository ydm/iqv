ONE = 'one@example.com'
TWO = 'two@example.com'


def create_contact(client, make_contact):
    obj = make_contact()
    resp = client.post('/contacts', data=obj)
    assert resp.status_code == 200
    return dict(resp.json['contact'])


def test_get_post(client, make_contact):
    contact = create_contact(client, make_contact)
    id_ = contact['id']
    uri = f'/contact/{id_}/emails'
    # POST an email
    first = client.post(uri, data={'emails': [ONE,]})
    assert first.status_code == 200
    assert len(first.json['emails']) == 1
    assert first.json['emails'][0] == {'email': ONE}
    # POST another one
    second = client.post(uri, data={'emails': [TWO,]})
    assert second.status_code == 200
    assert len(second.json['emails']) == 2
    assert second.json['emails'][0] == {'email': ONE}
    assert second.json['emails'][1] == {'email': TWO}
    # GET both
    third = client.get(uri)
    assert third.status_code == 200
    assert len(third.json['emails']) == 2
    assert third.json['emails'][0] == {'email': ONE}
    assert third.json['emails'][1] == {'email': TWO}


def test_put(client, make_contact):
    contact = create_contact(client, make_contact)
    id_ = contact['id']
    uri = f'/contact/{id_}/emails'
    resp = client.put(uri, data={'emails': [ONE, TWO]})
    assert resp.status_code == 200
    assert len(resp.json['emails']) == 2
    assert resp.json['emails'][0] == {'email': ONE}
    assert resp.json['emails'][1] == {'email': TWO}


def test_delete(client, make_contact):
    contact = create_contact(client, make_contact)
    id_ = contact['id']
    uri = f'/contact/{id_}/emails'
    # Put some emails
    put = client.put(uri, data={'emails': [ONE, TWO]})
    assert put.status_code == 200
    assert len(put.json['emails']) == 2
    # Make sure they exist
    before = client.get(uri)
    assert before.status_code == 200
    assert len(before.json['emails']) == 2
    # Delete them
    delete = client.delete(uri)
    assert delete.status_code == 200
    # Check if they are really dissociated from this contact (also
    # should be deleted, but we're unable to test this here).
    after = client.get(uri)
    assert after.status_code == 200
    assert len(after.json['emails']) == 0
