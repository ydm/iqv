def assert_eq(a, b):
    '''
    Make sure the data fields of dicts `a' and `b' are (==).
    '''
    fields = ('username', 'first_name', 'last_name')
    for x in fields:
        assert a[x] == b[x]


def create_some_contacts(client, make_contact, n):
    '''
    Mock some objects using the creation endpoint.
    '''
    xs = [make_contact(i) for i in range(n)]
    for x in xs:
        resp = client.post('/contacts', data=x)
        assert resp.status_code == 200
    return xs


def test_list(client, make_contact):
    '''
    Before creating any contacts, the contact_list returned should be
    empty and the total number of pages should be 0.

    After creating a few contacts, they should be listed on a single
    page.
    '''
    resp = client.get('/contacts')
    assert resp.status_code == 200
    assert len(resp.json['contact_list']) == 0
    assert resp.json['has_prev'] is False
    assert resp.json['has_next'] is False
    assert resp.json['total'] == 0
    # Create some contacts and fetch the list again
    create_some_contacts(client, make_contact, 5)
    resp = client.get('/contacts')
    assert resp.status_code == 200
    assert len(resp.json['contact_list']) == 5
    assert resp.json['has_prev'] is False
    assert resp.json['has_next'] is False
    assert resp.json['total'] == 1


def test_list_deep(client, make_contact):
    '''
    Create some contacts, then fetch them and inspect the contents of
    the returned list.
    '''
    xs = create_some_contacts(client, make_contact, 5)
    resp = client.get('/contacts')
    assert resp.status_code == 200
    assert len(resp.json['contact_list']) == 5
    for (actual, expected) in zip(resp.json['contact_list'], xs):
        assert_eq(actual, expected)


def test_pages(client, make_contact):
    '''
    Create 11 contacts and list them by 5 on page.  Expect 3 pages,
    5/5/1 items on each page respectively and proper has_prev and
    has_next properties.
    '''
    def f(page, expected, has_prev, has_next):
        resp = client.get(f'/contacts?per_page=5&page={page}')
        assert resp.status_code == 200
        assert len(resp.json['contact_list']) == expected
        assert resp.json['total'] == 3
        assert resp.json['has_prev'] == has_prev
        assert resp.json['has_next'] == has_next
    create_some_contacts(client, make_contact, 11)
    f(page=1, expected=5, has_prev=False, has_next=True)
    f(page=2, expected=5, has_prev=True,  has_next=True)
    f(page=3, expected=1, has_prev=True,  has_next=False)


def test_pages_deep(client, make_contact):
    '''
    Create 15 contacts, paginate them by 5 on page, fetch the 2nd page
    and inspect the items served.
    '''
    create_some_contacts(client, make_contact, 15)
    resp = client.get('/contacts?per_page=5&page=2')
    assert resp.status_code == 200
    assert len(resp.json['contact_list']) == 5
    xs = tuple(make_contact(i) for i in range(5, 10))
    for (actual, expected) in zip(resp.json['contact_list'], xs):
        assert_eq(actual, expected)


def test_nonexistent_page(client, make_contact):
    '''
    Status 404 should be served when trying to get nonexistent pages.
    '''
    nonexistent = (-20, -1, 0, 1, 20, 40, 100)
    for page in nonexistent:
        resp = client.get(f'/contact_list?page={page}')
        assert resp.status_code == 404


def test_search(client, make_contact):
    '''
    Create two sets of contacts -- the first five have an underscore
    in its name, the second five do not.  Then filter the list using
    the ?q= argument and make sure the proper contacts are retrieved.
    '''
    expected = ('one_', 'two_', 'three_', 'four_', 'five_')
    extra    = ('one' , 'two' , 'three' , 'four' , 'five' )
    for x in expected + extra:
        obj = make_contact(username=x)
        resp = client.post('/contacts', data=obj)
        assert resp.status_code == 200
    # Make sure all 10 contacts are created
    resp = client.get('/contacts?per_page=100')
    assert resp.status_code == 200
    assert len(resp.json['contact_list']) == 10
    # Fetch contacts that contain an underscore in the username
    #
    # First page: one_ and two_
    first = client.get('/contacts?q=_&per_page=2&page=1')
    assert len(first.json['contact_list']) == 2
    first.json['contact_list'][0]['username'] == expected[0]
    first.json['contact_list'][1]['username'] == expected[1]
    # Second page: three_ and four_
    second = client.get('/contacts?q=_&per_page=2&page=2')
    assert len(second.json['contact_list']) == 2
    second.json['contact_list'][0]['username'] == expected[2]
    second.json['contact_list'][1]['username'] == expected[3]
    # Fifth page: five_
    third = client.get('/contacts?q=_&per_page=2&page=3')
    assert len(third.json['contact_list']) == 1
    third.json['contact_list'][0]['username'] == expected[4]


def test_invalid_args(client, make_contact):
    '''
    Any invalid GET arguments should be ignored.
    '''
    create_some_contacts(client, make_contact, 10)
    resp = client.get('/contacts?per_page=5&page=1&this_is_invalid=something')
    assert resp.status_code == 200
