import os


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEFAULT_SECRET_KEY = 'w_&$9x#!uixu6vtgp&6hprbeyizankxxfwhnnf142**=1-(m9k'
DEFAULT_DATABASE_URI = 'sqlite:///{}'.format(
    os.path.join(BASE_DIR, 'db.sqlite3')
)


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY', DEFAULT_SECRET_KEY)
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        'DATABASE_URI', DEFAULT_DATABASE_URI
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False
