import datetime
import os
from random import choice
from string import ascii_letters

from celery import Celery

from iqv import boot
from iqv import (db, models)


app = Celery('tasks', broker=os.environ.get('BROKER_URI'))


def rands(n=4):
    '''
    Return a random string of length `n'.
    '''
    return ''.join(choice(ascii_letters) for _ in range(n))


@app.task
def create_random_contact():
    '''
    Create a contact (plus two associated emails), populated with
    random data.
    '''
    contact = models.Contact(
        username=rands(),
        first_name=rands(),
        last_name=rands()
    )
    randemail = lambda: '{}@{}.com'.format(rands(), rands())
    contact.append_emails([randemail(), randemail()])
    db.session.add(contact)
    db.session.commit()


@app.task
def delete_older_contacts():
    '''
    Delete contacts older than 1 minute.
    '''
    point = datetime.datetime.utcnow() - datetime.timedelta(minutes=1)
    xs = models.Contact.query.filter(models.Contact.created < point)
    for x in xs:
        x.delete_emails(db)
        db.session.delete(x)
    db.session.commit()


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    # Run the create_random_contact() task every 15 seconds
    sender.add_periodic_task(15.0, create_random_contact)
