import datetime
import re
from sqlalchemy import orm
from sqlalchemy.sql import func
from iqv import db


# https://emailregex.com/
EMAIL_RE = re.compile(r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$')
USERNAME_RE = re.compile(r'^[a-zA-Z0-9_]+$')
FORMAT = '%Y-%m-%d %H:%M:%S'


class Contact(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    created = db.Column(db.DateTime(timezone=True), server_default=func.now())

    username = db.Column(db.String(31), nullable=False, index=True)
    first_name = db.Column(db.String(31), nullable=False)
    last_name = db.Column(db.String(31), nullable=False)
    emails = db.relationship('Email')

    def to_dict(self):
        return {
            'id': self.id,
            'created': self.created.strftime(FORMAT),
            'username': self.username,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'emails': [e.email for e in self.emails]
        }

    def set_emails(self, emails):
        self.emails = [Email(email=x) for x in emails]

    def append_emails(self, emails):
        for x in emails:
            self.emails.append(Email(email=x))

    def delete_emails(self, db):
        xs = list(self.emails)
        self.set_emails([])
        for x in xs:
            db.session.delete(x)

    @orm.validates('username')
    def validate_username(self, key, value):
        # username is restricted to contain only latin alphabet
        # characters, numbers and underscores.
        #
        # We also check the length explicitly since sqlite doesn't
        # enforce it.
        assert (len(value) <= 31), key
        assert USERNAME_RE.match(value), key
        return value

    @orm.validates('first_name')
    def validate_first_name(self, key, value):
        # Non-empty first_name is required
        assert value, key
        assert (len(value) <= 31), key
        return value

    @orm.validates('last_name')
    def validate_last_name(self, key, value):
        good = value if value else ''
        assert (len(good) <= 31), key
        return good


class Email(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    # emails shouldn't be longer than 254:
    # http://www.rfc-editor.org/errata_search.php?rfc=3696&eid=1690
    email = db.Column(db.String(255), nullable=False)
    contact = db.Column(db.Integer, db.ForeignKey('contact.id'))

    def to_dict(self):
        return {
            'email': self.email
        }

    @orm.validates('email')
    def validate_email(self, key, value):
        assert EMAIL_RE.match(value), key
        return value
