import flask
from flask_restful import Resource
from flask_restful.reqparse import RequestParser
from iqv import models
from iqv import (api, db)


def make_emails_parser():
    p = RequestParser()
    p.add_argument('emails', required=True, action='append')
    return p


def make_list_parser():
    p = RequestParser(bundle_errors=True)
    p.add_argument('page', type=int, default=1)
    p.add_argument('per_page', type=int, default=10)
    p.add_argument('q')
    return p


def make_contact_parser(req):
    p = RequestParser(bundle_errors=True)
    p.add_argument('username', required=req)
    p.add_argument('first_name', required=req)
    p.add_argument('last_name', default='')
    p.add_argument('emails', action='append', default=[])
    return p


def num_pages(total, n):
    return total // n + (total % n > 0)


class Contacts(Resource):

    _list_parser = make_list_parser()
    _contact_parser = make_contact_parser(True)

    def get(self):
        '''
        Filter and list contacts.

        Query parameters:
        ?per_page -- how many items should be served per page
        ?page -- which page to show
        ?q -- username search string
        '''
        args = self._list_parser.parse_args()
        xs = models.Contact.query
        if args['q']:
            expr = models.Contact.username.contains(args['q'], autoescape=True)
            ys = xs.filter(expr)
        else:
            ys = xs
        p = ys.paginate(args['page'], args['per_page'])
        return {
            'contact_list': [obj.to_dict() for obj in p.items],
            'has_prev': p.has_prev,
            'has_next': p.has_next,
            'total': num_pages(p.total, args['per_page'])
        }

    def post(self):
        '''
        Create a new contact.
        '''
        data = self._contact_parser.parse_args(strict=True)
        emails = data.pop('emails')
        try:
            obj = models.Contact(**data)
            obj.set_emails(emails)
        except AssertionError as e:
            key = str(e)
            return ({'message': {key: f'Invalid {key} supplied'}}, 400)
        db.session.add(obj)
        db.session.commit()
        return {'contact': obj.to_dict()}


class Contact(Resource):

    _parser = make_contact_parser(False)

    def get(self, contact_id):
        '''
        Fetch a single contact.
        '''
        obj = models.Contact.query.get_or_404(contact_id)
        return {'contact': obj.to_dict()}

    def put(self, contact_id):
        '''
        Update an existing contact.
        '''
        obj = models.Contact.query.get_or_404(contact_id)
        data = self._parser.parse_args(strict=True)
        emails = data.pop('emails')
        if emails:
            obj.delete_emails(db)
            obj.set_emails(emails)
        for k, v in data.items():
            if v:
                setattr(obj, k, v)
        db.session.commit()
        return {'contact': obj.to_dict()}

    def delete(self, contact_id):
        '''
        Delete a contact.
        '''
        obj = models.Contact.query.get_or_404(contact_id)
        # Store data to be returned to client
        data = obj.to_dict()
        # Delete associated emails and the contact itself
        obj.delete_emails(db)
        db.session.delete(obj)
        # Commit and serve
        db.session.commit()
        return {'contact': data}


class Emails(Resource):

    _parser = make_emails_parser()

    def get(self, contact_id):
        '''
        Retrieve all emails, associated with the given contact.
        '''
        contact = models.Contact.query.get_or_404(contact_id)
        return {'emails': [x.to_dict() for x in contact.emails]}

    def post(self, contact_id):
        '''
        Add an email address to the list of email addresses of the
        given contact.
        '''
        contact = models.Contact.query.get_or_404(contact_id)
        data = self._parser.parse_args()
        contact.append_emails(data['emails'])
        db.session.commit()
        return {'emails': [x.to_dict() for x in contact.emails]}

    def put(self, contact_id):
        '''
        Replace the list of email addresses for the given contact.
        '''
        contact = models.Contact.query.get_or_404(contact_id)
        data = self._parser.parse_args()
        # Replace emails
        contact.delete_emails(db)
        contact.set_emails(data['emails'])
        # Commit changes and return
        db.session.commit()
        return {'emails': [y.to_dict() for y in contact.emails]}

    def delete(self, contact_id):
        '''
        Delete all email addresses, associated with the given contact.
        '''
        contact = models.Contact.query.get_or_404(contact_id)
        contact.delete_emails(db)
        db.session.commit()
        return {}


api.add_resource(Contacts, '/contacts')
api.add_resource(Contact, '/contact/<int:contact_id>')
api.add_resource(Emails, '/contact/<int:contact_id>/emails')
