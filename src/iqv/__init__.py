import flask
import flask_migrate
import flask_restful
import flask_sqlalchemy

import config


app = flask.Flask(__name__, static_url_path='')
app.config.from_object(config.Config)

api = flask_restful.Api(app)
db = flask_sqlalchemy.SQLAlchemy(app)
migrate = flask_migrate.Migrate(app, db)
