"""contacts table

Revision ID: 4caa2e00cd56
Revises: 
Create Date: 2019-12-16 21:17:39.335348

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4caa2e00cd56'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('contact',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('created', sa.DateTime(timezone=True), server_default=sa.text('(CURRENT_TIMESTAMP)'), nullable=True),
    sa.Column('username', sa.String(length=31), nullable=False),
    sa.Column('first_name', sa.String(length=31), nullable=False),
    sa.Column('last_name', sa.String(length=31), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_contact_username'), 'contact', ['username'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_contact_username'), table_name='contact')
    op.drop_table('contact')
    # ### end Alembic commands ###
